-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 04, 2018 at 04:34 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `roadviolations`
--

-- --------------------------------------------------------

--
-- Table structure for table `administrationusers`
--

CREATE TABLE `administrationusers` (
  `id` int(11) NOT NULL,
  `userName` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `administrationusers`
--

INSERT INTO `administrationusers` (`id`, `userName`, `password`) VALUES
(5, 'admin', '123');

-- --------------------------------------------------------

--
-- Table structure for table `information`
--

CREATE TABLE `information` (
  `id` int(11) NOT NULL,
  `time` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `vehicleNo` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `information`
--

INSERT INTO `information` (`id`, `time`, `location`, `vehicleNo`, `date`) VALUES
(1, '9.30', 'dehiwala', 'KJ0223', '2012.05.06'),
(2, '9.30', 'dehiwala', 'KJ0223', '2012.05.06'),
(3, 'Nugegoda', 'Ampara', 'JR300', '2012.06.05'),
(4, 'Nugegoda', 'Ampara', 'JR300', '2012.06.05'),
(5, '9.45', 'dehiwala', 'KJ0223', '2013.06.05'),
(6, '9.45', 'dehiwala', 'KJ0223', '2013.06.05'),
(7, '4.00', 'Jaffna', 'SR2', '2012.03.05'),
(8, '4.00', 'Jaffna', 'SR2', '2012.03.05'),
(9, '9.35', 'colombo', 'JS05', '2012.03.05'),
(10, '9.35', 'colombo', 'JS05', '2012.03.05'),
(11, '9.50', 'Ampara', 'KJ334', '2012.08.05'),
(12, '8.9', 'JAela', 'GR300', '2012.04.06'),
(13, '6.50', 'colombo', 'GR300', 'GR300'),
(14, '6.00', 'Kadana', 'GO334', '2013.03.05');

-- --------------------------------------------------------

--
-- Table structure for table `pastyears`
--

CREATE TABLE `pastyears` (
  `id` int(255) NOT NULL,
  `Year` int(255) NOT NULL,
  `Accidents` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pastyears`
--

INSERT INTO `pastyears` (`id`, `Year`, `Accidents`) VALUES
(1, 2010, 3004),
(2, 2011, 4000),
(3, 2012, 3000),
(4, 2014, 4000),
(5, 2015, 1090);

-- --------------------------------------------------------

--
-- Table structure for table `police`
--

CREATE TABLE `police` (
  `id` int(11) NOT NULL,
  `username` varchar(11) NOT NULL,
  `password` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `police`
--

INSERT INTO `police` (`id`, `username`, `password`) VALUES
(1, '123', '123'),
(3, 'nishika', '789'),
(2, 'thisal', '12345');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `administrationusers`
--
ALTER TABLE `administrationusers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `information`
--
ALTER TABLE `information`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vehicleNo` (`vehicleNo`);

--
-- Indexes for table `pastyears`
--
ALTER TABLE `pastyears`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `police`
--
ALTER TABLE `police`
  ADD PRIMARY KEY (`id`),
  ADD KEY `username` (`username`,`password`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `administrationusers`
--
ALTER TABLE `administrationusers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `information`
--
ALTER TABLE `information`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `pastyears`
--
ALTER TABLE `pastyears`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `police`
--
ALTER TABLE `police`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
