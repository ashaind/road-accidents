<?php

function getConnection(){
  $dbhost = 'localhost';
  $dbname = 'roadviolations';
  $dbuser = 'root';
  $dbpass = '';
  
  $mysqli = new mysqli($dbhost, $dbuser, $dbpass, $dbname);

  /* check connection */
  if ($mysqli->connect_errno) {
      throwError("Connection failed: " . $mysqli->connect_error);
      die();
  }
  return $mysqli;
}





function getRows($sql){
  $mysqli = getConnection();
  $result = $mysqli->query($sql);

  $rows = [];
  while($row = $result->fetch_array())
  {
    $rows[] = $row;
  }

  /* free result set */
  $result->free();

  /* close connection */
  $mysqli->close();

  /* return data */
  return $rows;
} 





  function throwError($msg){
    echo "<p>Server Error:<br>".$msg."</p>";
    die();
  }


  function getAccidents(){
    return json_encode(getRows("SELECT Year as `year`, Accidents as `accidents` FROM pastyears"));  
  }

  function getMonthlyStats(){
    $startDate = "2013-01-01";
    $endDate = "2018-08-01";
    $sql = "SELECT count(*) as accidents, location, date(timestamp) as date FROM `information` GROUP by location, timestamp having  timestamp BETWEEN '$startDate 00:00:01' AND '$endDate 23:59:59'";
    return json_encode(getRows($sql));
  }


?>