<?php require 'server.php'; ?>
<!DOCTYPE html>

<html>
  <head>
 
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> Road Accidents </title>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
  </head>
  <body>
          
    <h1>Display Mysql Data</h1>
    <div id="myfirstchart" style="height: 400px;"></div>

    <br>
    <br>

    <div id="mysecondchart" style="height: 400px;"></div>
     
     
     <script type="text/javascript">   
      new Morris.Bar({
        // ID of the element in which to draw the chart.
        element: 'myfirstchart',
        // Chart data records -- each entry in this array corresponds to a point on
        // the chart.
        data: <?php   echo getAccidents(); ?>,
        // The name of the data record attribute that contains x-values.
        xkey: 'year',
        // A list of names of data record attributes that contain y-values.
        ykeys: ['accidents'],
        // Labels for the ykeys -- will be displayed when you hover over the
        // chart.
        labels: ['Value']
      });


      new Morris.Bar({
        // ID of the element in which to draw the chart.
        element: 'mysecondchart',
        // Chart data records -- each entry in this array corresponds to a point on
        // the chart.
        data: <?php   echo getMonthlyStats(); ?>,
        // The name of the data record attribute that contains x-values.
        xkey: 'location',
        // A list of names of data record attributes that contain y-values.
        ykeys: ['accidents'],
        // Labels for the ykeys -- will be displayed when you hover over the
        // chart.
        labels: ['Value']
      });
   </script>
  </body>
</html>